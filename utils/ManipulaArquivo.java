package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ManipulaArquivo {
    private String filePath;

    public ManipulaArquivo(String path) {
        this.filePath = path;
    }

    public void escreveLinha(String conteudo) {
        try {
            File file = new File(filePath);

            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(conteudo);
            bw.newLine();
            bw.close();
            fw.close();
        } catch (Exception e) {
            System.out.println("Erro ao tentar criar o arquivo");
        }


    }
}
