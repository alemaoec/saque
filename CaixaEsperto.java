import operacoes.Deposito;
import operacoes.Operacao;
import operacoes.Saque;

import java.util.*;

public class CaixaEsperto {
    Operacao[] listaOperacoes;
    double saldoAtual;
    int operacoes;

    public CaixaEsperto(double saldoInicial) {
        this.saldoAtual = saldoInicial;
        listaOperacoes = new Operacao[1000];
        operacoes = 0;
    }

    public void fazerSaque(double valor) {
        try {
            Operacao saque = new Saque(saldoAtual);
            this.saldoAtual = saque.executa(valor);
            listaOperacoes[operacoes] = saque;
            operacoes ++;
        } catch(Exception e) {
            System.out.println("Saldo inferior ao saque");
        }
    }

    public void fazerDeposito(double valor) {
        try {
            Operacao deposito = new Deposito(saldoAtual);
            this.saldoAtual = deposito.executa(valor);
            listaOperacoes[operacoes] = deposito;
            operacoes ++;
        } catch(Exception e) {
            System.out.println("Ocorreu algum erro ao fazer o deposito");
        }
    }


}
