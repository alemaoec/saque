package operacoes;

public abstract class Operacao {
    double saldoAtual, valor;

    public Operacao(double saldo){
        this.saldoAtual = saldo;
    }

    public abstract String escreveExtrato();

    public abstract double executa(double valor) throws CaixaException;
}
