package operacoes;

public class Saque extends Operacao{
    public Saque(double saldo){
        super(saldo);
    }

    @Override
    public String escreveExtrato() {
        return "-" + valor;
    }

    public double executa(double valor) throws CaixaException {
        if(valor > saldoAtual) {
            throw new CaixaException("Saldo insuficiente");
        }
        this.valor = valor;
        return saldoAtual - valor;
    }
}
