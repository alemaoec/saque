package operacoes;

public class Deposito extends Operacao {

    public Deposito(double saldo){
        super(saldo);
    }

    @Override
    public double executa(double valor) {
        this.valor = valor;
        return saldoAtual + valor;
    }
    
    @Override
    public String escreveExtrato() {
        return "+" + valor;
    }
}
